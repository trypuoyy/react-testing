FROM node
WORKDIR /app
COPY . ./
EXPOSE 3000
CMD ["npx", "run", "-s", "build"]
